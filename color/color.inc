<?php
/*
 * @file color.inc
 * Implimentaion of color.inc for Pixture Reloaded
 */

$info = array(
  // Fields
  'fields' => array(
    'base'   => t('Base'),
    'link'   => t('Links'),
    'top'    => t('Header'),
    'bottom' => t('Footer'),
    'text'   => t('Text'),
  ),
  // Pre-defined color schemes.
  'schemes' => array(
    'default' => array(
      'title' => t('Girly Pink (default)'),
      'colors' => array(
        'base' => '#eb52c1', 'link' => '#b21f88', 'top' => '#b800a6', 'bottom' => '#ff6bfe', 'text' => '#555555',
      ),
    ),
    'ash' => array(
      'title' => t('Ash'),
      'colors' => array(
        'base' => '#464849', 'link' => '#2f416f', 'top' => '#2a2b2d', 'bottom' => '#5d6779', 'text' => '#555555',
      ),
    ),
    'aquamarine' => array(
      'title' => t('Aquamarine'),
      'colors' => array(
        'base' => '#55c0e2', 'link' => '#000000', 'top' => '#085360', 'bottom' => '#007e94', 'text' => '#555555',
      ),
    ),
  ),

    // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => array(
    'css/colors.css',
  ),

  // Images to copy over
  'copy' => array(
    'css/images/large-alert.png',
    'css/images/bullet-round.png',
    'css/images/bullet-sm-arrow-right.png',
    'css/images/bullet-sm-arrow-down.png',
    'css/images/button.gif',
    'css/images/button-o.gif',
    'css/images/teaser-tl.png',
    'css/images/teaser-tr.png',
    'css/images/teaser-bl.png',
    'css/images/teaser-br.png',
    'css/images/logo.png',
  ),

  // Coordinates of gradient (x, y, width, height)
  'gradients' => array(
    array(
      'dimension' => array(0,0,800,94),
      'direction' => 'vertical',
      'colors' => array('top', 'bottom'),
    ),
  ),

  // Color areas to fill (x, y, width, height)
  'fill' => array(
    'base' => array(0,0,800,570),
    'link' => array(240,530,40,40),
  ),

  // Coordinates of all the theme slices (x, y, width, height).
  'slices' => array(
    'css/images/header.png' => array(0,0,512,88),
    'css/images/bg-header.png' => array(792,0,8,120),
    'css/images/bg-footer.png' => array(792,0,8,88),
    'css/images/bg-menu.png' => array(0,88,8,32),
    'css/images/bg-title.png' => array(20,130,10,10),
    'css/images/bg-wall.png' => array(792,120,8,408),
    'css/images/bg-bar-lite.png' => array(0,530,8,40),
    'css/images/bg-bar.png' => array(20,530,8,40),
    'css/images/bg-lbar-lite.png' => array(270,530,8,40),
    'css/images/bg-lbar.png' => array(250,530,8,40),
    'css/images/block-tl.png' => array(10,130,150,30),
    'css/images/block-tr.png' => array(20,130,210,30),
    'css/images/block-bl.png' => array(10,330,150,10),
    'css/images/block-br.png' => array(20,330,210,10),
    'css/images/block-tile.png' => array(20,330,10,10),
    'css/images/sticky-tl.png' => array(250,130,10,10),
    'css/images/sticky-tr.png' => array(770,130,10,10),
    'css/images/sticky-bl.png' => array(250,320,10,10),
    'css/images/sticky-br.png' => array(770,320,10,10),
    'css/images/sticky-tile.png' => array(260,130,10,10),
    'css/images/sf-hover-tile.png' => array(0,30,250,1),
    'screenshot.png' => array(0,0,700,420),
  ),

  // Reference color used for blending. Matches the base.png's colors.
  'blend_target' => '#ffffff',

  // Preview files.
  'preview_image' => 'color/preview.png',
  'preview_css' => 'color/preview.css',

  // Base file for image generation
  'base_image' => 'color/base.png',
);